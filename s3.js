var AWS = require('aws-sdk');
    AWS.config.region = 'us-east-1';

function S3(config){

}

S3.prototype = {
    upload: function(bucket, key, pipe, cb){
        var self = this;
        var s3obj = new AWS.S3({params: {Bucket: bucket, Key: key}});
            s3obj.upload({Body: pipe})
              .on('httpUploadProgress', function(evt) { console.log(evt); })
              .send(function(err, data) { 
                  if(err){
                    cb(err);
                  } else {
                    cb(null, data);   
                  }
              });
    }

}

module.exports = new S3();
