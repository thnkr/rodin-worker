var fs = require('fs');
var http = require('http');
var random_ua = require('random-ua');
var Url = require('url');
var child = require('child_process');
var zlib = require('zlib');
var request = require('request');
var unirest = require('unirest');
var ee = require('events').EventEmitter;
var moment = require('moment');
var events = new ee();
var socket = require('socket.io-client').connect("http://rodin.mdn1.com:6600");
var keys = fs.readFileSync("keys.txt", "utf8");
var PUBLIC_KEY = keys.split("\n")[0];
var PRIVATE_KEY = keys.split("\n")[1];
var os = require('os');
var hostname = os.hostname();
var TOTAL_REQUESTS = 0;
var AWS = require('aws-sdk');
    AWS.config.region = 'us-east-1';
    AWS.config.update({accessKeyId: PUBLIC_KEY, secretAccessKey: PRIVATE_KEY});

process.on("uncaughtError", function(err){
    console.log(err);
});

var config = {
    "redis_queue": {
        "host": "rodin.mdn1.com",
        "port": 6379
    },
    "rodin_queue": {
        "host": "rodin.mdn1.com",
        "port": 6600
    },
    "s3": {
        "bucket": "rodin-glacier-1"
    }
}

var registration = {
    host: hostname,
    id: socket.id,
    uptime: os.uptime(),
    platform: os.platform()+"-"+os.release(),
    freeMemory: os.freemem()
}

function log(msg){
    console.log("WORKER | "+moment().format("MM/DD-HH:mm:ss")+" "+msg);
}


function send(type, msg){
    socket.emit(type, msg);
}

function Downloader(){
    var self = this;
    this.registered = false;
    this.busy = false;
    this.proxies = [];
    this.refreshQueueSleep = 10;
}

Downloader.prototype = {

    getProxies: function(cb){
        var self = this;
        var url = self.queueUrl+"/i";
        request({ url: url}, function(err, response, body){
            if(err){
                console.log(err);
                cb(err);
            } else {
                self.proxies = JSON.parse(body);
                console.log("proxies");
                console.log(JSON.parse(body));
                cb(null, true);
            }
        }); 
    },

    dump: function(data){
        fs.writeFileSync(id+".tar.gz", data);
    },

    setConfig: function(config){
        var self = this;
        self.queueUrl = "http://"+config['rodin_queue']['host']+":"+config['rodin_queue']['port'];
        self.config = config;
    },

    headers: function(options){
        var options = {
                "Host": "www.google.com",
                "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
                "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.111 Safari/537.36",
                "Accept-Encoding": "l",
                "Accept-Language": "en-US,en;q=0.5",
                "Cache-Control": "no-cache",
            }
        return options; 
    },

    getTaskFrom: function(type, cb){
        var self = this;
        var url = self.queueUrl+"/q/"+type;
        var options = {
            "url": url,
            "method": "GET",
            "headers": self.headers()
        }
        request(options, function(err, response, body){
           if(err){
              send("error", err);
              console.log(err);
              cb(err);
           } else {
               try {
                    cb(null, JSON.parse(body));
               } catch (err){
                    cb(err);
               }
           }
        });
    },

    completeTask: function(task){

        console.log(task.id);
        var self = this;
        var url = self.queueUrl+"/q/done/"+task.id;

        var options = {
            "url": url,
            "method": "GET",
            "headers": self.headers()
        }

        send("message", { "type": "done", "id": task.id });

        request(options, function(err, response, body){
           if(err){
               console.log(err);
           } else {
                log("done: "+task.id);
           }
        });

    },


    failTask: function(task, err){

        console.log(task.id);
        var self = this;
        var url = self.queueUrl+"/q/retry/"+task.id;
        console.log(url);
        var options = {
            "url": url,
            "method": "GET",
            "headers": self.headers()
        }

        send("message", { "type": "retry", "id": err });

        request(options, function(err, response, body){
           if(err){
               console.log(err);
           } else {
                log("retry: "+task.id);
           }
        });

    },

    newTask: function(task, err){

        console.log(task.id);
        var self = this;
        var url = self.queueUrl+"/q/new/"+task.id;
        console.log(url);
        var options = {
            "url": url,
            "method": "GET",
            "headers": self.headers()
        }

        send("message", { "type": "retry-new", "id": err });

        request(options, function(err, response, body){
                    child.exec("sudo reboot -f", function(err, stderr, stdout){
                        if(err){
                            console.log(err);
                        } else {
                            process.exit();
                        }	
                    });

        });

    },


    download: function(options, cb){      
        var self = this;

        try {

        send("message", { "type": "new", "id": options.url });
		var Request = unirest.get(options.url)
		Request.proxy("http://127.0.0.1:58881");

		Request.headers({
			"Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
			"User-Agent": random_ua.generate(), 
			"Accept-Language": "en-US,en;q=0.5",
			"Cache-Control": "no-cache",
            "Connection": "close"
		});

		Request.end(function(response){

				var body = response.body;
				    log('uploading '+options['id']);

            var key = options['project']+"/"+options['id'];
            log('downloading '+key);
            var s3obj = new AWS.S3({params: {Bucket: "rodin-glacier-1" }});

				    var params = {
                        Key: key,
                        Body: response.body
				    }

				    s3obj.upload(params, function(err, data){
					  if(err){
					    console.log("S3 downloader error");
					    cb(err);
					  } else {
					    cb(null, response.body);   
					  }
				    });

		});


        } catch (err){
            cb(err);
        }

    },


    sleep: function(time, wakeup){
        var self = this;
            self.busy = true;

        var t = time*100;
        setTimeout(function(){
            self.busy = false;    
            wakeup();
        }, 900);
    }

}

var worker = new Downloader();

socket.on('connect', function(){
  socket.emit("register", registration); 
});

function controller(){

    if(worker.busy == false){

        worker.getTaskFrom("new", function(err, task){
            if(err){

                console.log(err);
                send("error", err);

            } else {
                
                if(task['id'] != undefined){

                  log("new task "+task['id']);
                  var id = task['id'];

     		      TOTAL_REQUESTS+=1;

                  worker.download(task, function(err, pipe){

                      if(err){
                        log("Error retrying task...");
                        console.log(err);

                        worker.failTask(task, err);
                        worker.sleep(task['sleep'], controller);

                      } else {

			            task.body = pipe;
                        if(task.body.indexOf("quota limit") > -1 || task.body.indexOf("504 Gateway Time-out") > -1){

                            worker.newTask(task);

                        } else {

                            worker.completeTask(task);
                            worker.sleep(task['sleep'], controller);

                        }

                      }

                  if(TOTAL_REQUESTS > 9){

                    child.exec("sudo reboot -f", function(err, stderr, stdout){
                        if(err){
                            console.log(err);
                        } else {
                            process.exit();
                        }	
                    });

                  }


                  });

                } else {

                  log("No new tasks, sleeping ");
                  worker.sleep(10, controller); 

                }
            }
        });

    } else {
       log('busy'); 
    }
}

worker.setConfig(config);
worker.registered = true;

setTimeout(function(){


        controller();


}, 2000);



socket.on('disconnect', function(){
   log('ERROR : LOST CONNECTION WITH HOST');
   process.exit();

});

var commands = {
    "kill": function(){
      process.exit(); 
    },
    "stop": function(){
      worker.busy = true;
    },
    "start": function(){
      worker.busy = false;
    }
}

socket.on('command', function(command){

    log('recieved command "'+command);
    if(commands[command] != undefined){
        commands[command]();
    }

});


//function onRequest(client_req, client_res) {
//
//  console.log('serve: ' + client_req.url);
//  console.log(client_req.url); 
//
//  var options = {
//    hostname: "localhost",
//    port: 5566,
//    path: client_req.url,
//    method: 'GET'
//  };
//  
//  var proxy = http.request(options, function (res) {
//    res.pipe(client_res, {
//      end: true
//    });
//  });
//
//  client_req.pipe(proxy, {
//    end: true
//  });
//}
//
//http.createServer(onRequest).listen(6600);
