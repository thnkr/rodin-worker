#!/bin/bash
sudo /usr/bin/docker run -d -p 58881:58881 -p 1936:1936 --env tors=30 mattes/rotating-proxy
sleep 2
cd rodin-worker
/usr/local/bin/forever downloader.js &
sleep 490
sudo reboot -f
exit